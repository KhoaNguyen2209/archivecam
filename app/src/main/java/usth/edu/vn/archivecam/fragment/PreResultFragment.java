package usth.edu.vn.archivecam.fragment;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.io.FileNotFoundException;
import java.io.InputStream;

import usth.edu.vn.archivecam.R;
import usth.edu.vn.archivecam.loadImage;

/**
 * Created by Khoa on 26-May-16.
 */
public class PreResultFragment extends Fragment {
    public PreResultFragment(){}

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_preresult, null);

        ImageView iv = (ImageView)v.findViewById(R.id.pre_result_image);

        new loadImage(getActivity(), iv).execute("preResult.png");
        return v;
    }
}
