package usth.edu.vn.archivecam.fragment;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.io.FileNotFoundException;
import java.io.InputStream;

import usth.edu.vn.archivecam.MainActivity;
import usth.edu.vn.archivecam.R;
import usth.edu.vn.archivecam.loadImage;

/**
 * Created by Khoa on 26-May-16.
 */
public class PhotoFragment extends Fragment {
    public PhotoFragment(){}
    Activity activity = getActivity();
    private ImageView iv;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_photo, null);

        ImageView iv = (ImageView)v.findViewById(R.id.photo_image);

        new loadImage(getActivity(), iv).execute("photo.png");
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
}
