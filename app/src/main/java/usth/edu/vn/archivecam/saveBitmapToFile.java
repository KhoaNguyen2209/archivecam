package usth.edu.vn.archivecam;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by Khoa on 28-May-16.
 */
public class saveBitmapToFile {

    public saveBitmapToFile(){

    }

    protected void saveToInternal(Context context, Bitmap bitmap, String fileName) {
        long start = System.currentTimeMillis();
        FileOutputStream fos;
        try{
            fos = context.openFileOutput(fileName, Context.MODE_PRIVATE);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 70, fos);
            fos.close();
        }catch (FileNotFoundException e){
            Log.d("Save File", "File not found");
            e.printStackTrace();
        }catch (IOException e){
            Log.d("SaveFile", "IO Exception");
            e.printStackTrace();
        }
        long end = System.currentTimeMillis();
        System.out.println(end - start);
    }

}

