package usth.edu.vn.archivecam;

import android.graphics.Bitmap;
import android.graphics.Matrix;

import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.opencv.utils.Converters;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Khoa on 16-May-16.
 */
public class documentFinder {
    private Mat inputFrame = new Mat();
    public documentFinder(){}
    private List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
    private int largest_contour_index = 0;
    private Point[] pointRect;
    private int numPoint;

    private int A4_WIDTH = 1240;
    private int A4_HEIGHT = 1754;

    public Bitmap documentDetect(Bitmap bm){
        Mat inputFrameClone;

        Bitmap result = Bitmap.createBitmap(bm.getWidth(), bm.getHeight(), Bitmap.Config.RGB_565);
        Mat temp = new Mat();
        Mat hierarchy = new Mat();

        double current_area_size;
        double largest_size = 0;

        Utils.bitmapToMat(bm, inputFrame);

        inputFrameClone = inputFrame.clone();

        Imgproc.cvtColor(inputFrame, temp, Imgproc.COLOR_RGB2GRAY);

        Imgproc.threshold(temp, temp, -1, 255, Imgproc.THRESH_BINARY+Imgproc.THRESH_OTSU);
        Imgproc.GaussianBlur(temp, temp, new Size(5, 5), 0);
        Imgproc.medianBlur(temp, temp, 19);

        Imgproc.findContours(temp, contours, hierarchy, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_NONE );

        for (int i = 0; i < contours.size(); i++){
            current_area_size = Imgproc.contourArea(contours.get(i));
            if(current_area_size > largest_size){
                largest_size = current_area_size;
                largest_contour_index = i;
            }
        }

        MatOfPoint2f point32f = new MatOfPoint2f();

        contours.get(largest_contour_index).convertTo(point32f, CvType.CV_32F);

        Imgproc.approxPolyDP(point32f, point32f, Imgproc.arcLength(point32f, true) * 0.02, true);

        point32f.convertTo(contours.get(largest_contour_index), CvType.CV_32S);

        Imgproc.drawContours(inputFrameClone, contours, largest_contour_index, new Scalar(255, 0, 0), 5);

        Utils.matToBitmap(inputFrameClone, result);

        Matrix mat = new Matrix();
        mat.postRotate(90);

        pointRect = contours.get(largest_contour_index).toArray();
        numPoint = pointRect.length;

        Bitmap resultRotate = Bitmap.createBitmap(result, 0, 0, result.getWidth(), result.getHeight(), mat, true);

        return resultRotate;
    }

    public Mat getInputFrame() {
        return inputFrame;
    }

    public List<MatOfPoint> getContours() {
        return contours;
    }

    public Point[] getPointRect() {
        return pointRect;
    }

    public int getNumPoint() {
        return numPoint;
    }

    public int getLargest_contour_index() {
        return largest_contour_index;
    }

    public Bitmap documentExtractor(Mat inputFrame, List<MatOfPoint> contours, int largest_contour_index, Point[] pointRect, int numPoint){
        int dstWidth = A4_WIDTH;
        int dstHeight = A4_HEIGHT;

        Mat result = new Mat(dstWidth, dstHeight, CvType.CV_8UC3);

//        pointRect = contours.get(largest_contour_index).toArray();
//        numPoint = pointRect.length;
        List<Point> listCorner = new ArrayList<Point>();

        for(int i = 0; i < numPoint; i++){
            listCorner.add(pointRect[i]);
        }

        Mat source = Converters.vector_Point2f_to_Mat(listCorner);

        List<Point> dstCorner = new ArrayList<Point>();
        dstCorner.add(new Point(0,0));
        dstCorner.add(new Point(0,dstHeight));
        dstCorner.add(new Point(dstWidth, dstHeight));
        dstCorner.add(new Point(dstWidth, 0));

        Mat designate = Converters.vector_Point2f_to_Mat(dstCorner);

        Mat transform = Imgproc.getPerspectiveTransform(source, designate);

        Imgproc.warpPerspective(inputFrame, result, transform, new Size(dstWidth, dstHeight));

        result.convertTo(result, -1, 1.25, 0);

        Imgproc.cvtColor(result, result, Imgproc.COLOR_RGB2GRAY);

        Imgproc.equalizeHist(result, result);

//        result.convertTo(result, -1, 1.5, 0);

//        Imgproc.threshold(result, result, 200, 255, Imgproc.THRESH_BINARY);

        Bitmap resultBitmap = Bitmap.createBitmap(dstWidth, dstHeight, Bitmap.Config.RGB_565);

        Utils.matToBitmap(result, resultBitmap);

        Matrix mat = new Matrix();
        mat.postRotate(180);

        Bitmap resultRotate = Bitmap.createBitmap(resultBitmap, 0, 0, resultBitmap.getWidth(), resultBitmap.getHeight(), mat, true);

        return resultRotate;
    }

    public boolean checkRect(int numPoint){
        if(numPoint != 4){
            return false;
        }else{
            return true;
        }
    }
}
